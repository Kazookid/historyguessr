// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getStorage } from "firebase/storage";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCPeliRVIKkA2CLOTqfzkmBW6S7IDa-EkE",
  authDomain: "historyguessr.firebaseapp.com",
  projectId: "historyguessr",
  storageBucket: "historyguessr.appspot.com",
  messagingSenderId: "879586632466",
  appId: "1:879586632466:web:97f95a2713d437385a09cb",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const storage = getStorage(app);
